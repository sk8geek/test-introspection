package uk.co.channele.test.intro;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;
import uk.co.channele.test.maven.domain.Student;

import javax.inject.Inject;
import java.util.List;

@MicronautTest
public class IntroTest {

    @Inject
    @Client("/students")
    HttpClient client;

    @Test
    void testGetPasses() {

        List<Student> response = client.toBlocking().retrieve("/", List.class);

        assert(response instanceof List);
        assert(response.size() == 0);

    }

    @Test
    void testPostFails() throws JsonProcessingException {

        Student student = new Student();
        student.setName("Alice");
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(student);

        HttpRequest request = HttpRequest.POST("/", json);
        HttpResponse<Void> response = client.toBlocking().exchange(request);

        assert(response.getStatus() == HttpStatus.NO_CONTENT);

    }


}
