package uk.co.channele.test.intro.repo;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;
import uk.co.channele.test.maven.domain.Student;

@Repository
public abstract class StudentRepo implements CrudRepository<Student, Long> {

}
