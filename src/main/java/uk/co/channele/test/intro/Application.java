package uk.co.channele.test.intro;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.runtime.Micronaut;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

//@Introspected(classes = {uk.co.channele.test.maven.domain.Student.class}) // also fails
//@Introspected(packages = "uk.co.channele.test.maven.domain", includedAnnotations = {Entity.class}) // also fails
//@Introspected(packages = "uk.co.channele.test.maven.domain", includedAnnotations = {Column.class, Id.class})
@Introspected(packages = "uk.co.channele.test.maven.domain", includedAnnotations = {Entity.class, Column.class, Id.class, GeneratedValue.class})
public class Application {

    public static void main(String[] args) {
        Micronaut.run(Application.class);
    }
}