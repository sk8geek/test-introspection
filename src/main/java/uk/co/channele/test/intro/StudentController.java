package uk.co.channele.test.intro;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import uk.co.channele.test.intro.repo.StudentRepo;
import uk.co.channele.test.maven.domain.Student;

import javax.inject.Inject;
import javax.persistence.Column;
import javax.persistence.Id;

@Introspected(packages = "uk.co.channele.test.maven.domain", includedAnnotations = {Column.class, Id.class})
@Controller("/students")
public class StudentController {

    @Inject
    private StudentRepo repo;

    @Get()
    @ExecuteOn(TaskExecutors.IO)
    public HttpResponse<Iterable<Student>> index() {
        return HttpResponse.ok(repo.findAll());
    }

    @Post()
    @ExecuteOn(TaskExecutors.IO)
    public HttpResponse<Void> add(@Body String name) {
        Student student = new Student();
        student.setName(name);
        repo.save(student);
        return HttpResponse.noContent();
    }

}
