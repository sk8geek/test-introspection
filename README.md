# @Introspected Problem

Introspection of imported files doesn't seem to work as documented.

A `POST` call to `/students/` causes a `400` response with the message 
```
Cannot validate uk.co.channele.test.maven.domain.Student. 
No bean introspection present. Please add @Introspected to 
the class and ensure Micronaut annotation processing is enabled
```

I have tried adding 
```
@Introspected(packages = "uk.co.channele.test.maven.domain", includedAnnotations = {Column.class, Id.class})
```
in various places, `Application`, `AppConfig` and `StudentController` but to 
no avail.

